package ru.tsc.denisturovsky.tm.api.model;

import ru.tsc.denisturovsky.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
