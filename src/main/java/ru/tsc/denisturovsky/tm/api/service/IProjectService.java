package ru.tsc.denisturovsky.tm.api.service;


import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project updateOneById(String id, String name, String description);

    Project updateOneByIndex(Integer index, String name, String description);

}
