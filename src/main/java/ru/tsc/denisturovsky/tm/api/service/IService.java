package ru.tsc.denisturovsky.tm.api.service;

import ru.tsc.denisturovsky.tm.api.repository.IRepository;
import ru.tsc.denisturovsky.tm.enumerated.Sort;
import ru.tsc.denisturovsky.tm.model.AbstractModel;
import java.util.List;

public interface IService<M extends AbstractModel> extends IRepository<M> {

    List<M> findAll(Sort sort);

}
