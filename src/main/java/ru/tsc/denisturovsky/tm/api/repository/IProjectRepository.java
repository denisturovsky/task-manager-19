package ru.tsc.denisturovsky.tm.api.repository;

import ru.tsc.denisturovsky.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    Project create(String name);

    Project create(String name, String description);

}
