package ru.tsc.denisturovsky.tm.service;

import ru.tsc.denisturovsky.tm.api.repository.IProjectRepository;
import ru.tsc.denisturovsky.tm.api.service.IProjectService;
import ru.tsc.denisturovsky.tm.enumerated.Status;
import ru.tsc.denisturovsky.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.denisturovsky.tm.exception.field.DescriptionEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.IdEmptyException;
import ru.tsc.denisturovsky.tm.exception.field.IndexIncorrectException;
import ru.tsc.denisturovsky.tm.exception.field.NameEmptyException;
import ru.tsc.denisturovsky.tm.model.Project;
import java.util.Date;

public final class ProjectService extends AbstractService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return repository.create(name);
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return repository.create(name, description);
    }

    @Override
    public Project create(final String name, final String description, final Date dateBegin, final Date dateEnd) {
        final Project project = create(name, description);
        if (project == null) throw new ProjectNotFoundException();
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project updateOneById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateOneByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index >= getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

}
